﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ДЗ6
{
    public class Program
    {
        public class User
        {
            public int Id { get; set; }
            public string Name { get; set; }
            public string Fam { get; set; }
            public string Otch { get; set; }
            public string Tel { get; set; }
            public string Pasport { get; set; }
            public string Data { get; set; }
            public string Login { get; set; }
            public int Password { get; set; }
        }
        public class Account
        {
            public int Id { get; set; }
            public string DataCh { get; set; }
            public int Summa { get; set; }
            public int IdUser { get; set; }
        }
        public class History
        {
            public int Id { get; set; }
            public string DataOper { get; set; }
            public string TipOper { get; set; }
            public int SummaOper { get; set; }
            public int IdCh { get; set; }
        }
        public static List<User> UserList()
        {
            return new List<User>
            {
                {new User(){Id = 1, Fam = "Смирнов", Name = "Иван", Otch = "Николаевич", Tel = "8-957-845-6985", Pasport = "1254 857412", Data = "01.09.2009", Login = "SIN", Password = 5687}},
                {new User(){Id = 2, Fam = "Сидоров", Name = "Николай", Otch = "Сергеевич", Tel = "8-957-845-2587", Pasport = "0584 659874", Data = "01.12.2012", Login = "SNS", Password = 2684}}
            };
        }
        public static List<Account> AccountList()
        {
            return new List<Account>
            {
                {new Account(){Id = 1, DataCh = "15.02.2015", Summa = 506, IdUser = 2}},
                {new Account(){Id = 2, DataCh = "10.12.2016", Summa = 10254, IdUser = 1}},
                {new Account(){Id = 3, DataCh = "25.04.2014", Summa = 1580, IdUser = 1}},
                {new Account(){Id = 4, DataCh = "17.03.2017", Summa = 50, IdUser = 2}}
            };
        }
        public static List<History> HistoryList()
        {
            return new List<History>
            {
                {new History(){Id = 1, DataOper = "16.02.2018", TipOper = "Пополнение", SummaOper = 200, IdCh = 3}},
                {new History(){Id = 2, DataOper = "18.03.2018", TipOper = "Снятие", SummaOper = 250, IdCh = 2}},
                {new History(){Id = 3, DataOper = "05.07.2018", TipOper = "Снятие", SummaOper = 300, IdCh = 4}},
                {new History(){Id = 4, DataOper = "24.02.2018", TipOper = "Пополнение", SummaOper = 270, IdCh = 1}}
            };
        }
        public static void Main()
        {
            Console.WriteLine("База данных клиентов банка.");
            Console.WriteLine("Введите логин:");
            string log = Console.ReadLine();

            Console.WriteLine("Введите пароль:");
            int pas = Convert.ToInt32(Console.ReadLine());
           
            List<User> users = UserList();
            
            var poisk = from User1 in users
                        where User1.Login.ToLower() == log
                        where User1.Password == pas
                        select User1;

            foreach (User User1 in poisk)
            {
                Console.WriteLine("№" + User1.Id + " ФИО: " + User1.Fam + " " + User1.Name + " " + User1.Otch + " тел.:" + User1.Tel + " паспорт:" + User1.Pasport + " дата регистрации:" + User1.Data);
            }
            
            Console.WriteLine("Для получения данных о счетах введите № клиента банка:");
            int nomer = Convert.ToInt32(Console.ReadLine());
            
            List<Account> accounts = AccountList();

            var scheta = from Account1 in accounts
                         where Account1.IdUser == nomer
                         select Account1;
            foreach(Account Account1 in scheta)
            {
                Console.WriteLine("№ счета:" + Account1.Id + " дата открытия:" + Account1.DataCh + " сумма:" + Account1.Summa);
            }
            
            Console.WriteLine("Для получения данных об операциях счета введите № счета:");
            int oper = Convert.ToInt32(Console.ReadLine());
   
            List<History> historys = HistoryList();
            /*
            var istor = from History1 in historys
                        where History1.IdCh == oper
                         select History1;
            foreach (History History1 in istor)
            {
                Console.WriteLine("№ операции:" + History1.Id + " дата операции:" + History1.DataOper + " тип операции: " + History1.TipOper + " сумма:" + History1.SummaOper);
            }
            */
            var historyaccount = historys.Join(accounts,
                h => h.IdCh,
                a => a.Id,
                (h, a) => new {Id = a.Id, DataCh = a.DataCh, Summa = a.Summa, IdUser = a.IdUser, id1 = h.Id, DataOper = h.DataOper, TipOper = h.TipOper, SummaOper = h.SummaOper, IdCh = h.IdCh});

            var scheta1 = from Account2 in historyaccount
                          where Account2.IdCh == oper
                          select Account2;

            foreach (var chis in scheta1)
            {
                Console.WriteLine($"№ счета:{chis.Id} | дата открытия:{chis.DataCh} | сумма:{chis.Summa} | № операции:{chis.id1} | дата операции:{chis.DataOper} | тип операции: {chis.TipOper} | сумма операции:{chis.SummaOper}");
            }

            var historyaccountuser = historyaccount.Join(users,
                ha => ha.IdUser,
                u => u.Id,
                (ha, u) => new { Id = ha.Id, DataCh = ha.DataCh, Summa = ha.Summa, IdUser = ha.IdUser, id1 = ha.id1, DataOper = ha.DataOper, TipOper = ha.TipOper, SummaOper = ha.SummaOper, IdCh = ha.IdCh,
                                 id2 = u.Id, Fam = u.Fam, Name = u.Name, Otch = u.Otch, Tel = u.Tel, Pasport = u.Pasport, Data = u.Data});

            Console.WriteLine("Для получения данных об операциях пополнения счетов введите 1, для снятия 2:");
            int ps = Convert.ToInt32(Console.ReadLine());

            if(ps == 1)
            {
                var operacii = from o in historyaccountuser
                               where o.TipOper == "Пополнение"
                               select o;
                foreach (var pop in operacii)
                {
                    Console.WriteLine($"№ операции:{ pop.id1} | дата операции: { pop.DataOper} | тип операции: { pop.TipOper} | сумма операции: { pop.SummaOper} | владелец счета, ФИО: {pop.Fam} {pop.Name} {pop.Otch}");
                }
            }
            if(ps == 2)
            {
                var operacii = from o in historyaccountuser
                               where o.TipOper == "Снятие"
                               select o;
                foreach (var pop in operacii)
                {
                    Console.WriteLine($"№ операции:{ pop.id1} | дата операции: { pop.DataOper} | тип операции: { pop.TipOper} | сумма операции: { pop.SummaOper} | владелец счета, ФИО: {pop.Fam} {pop.Name} {pop.Otch}");
                }
            }

            Console.WriteLine("Вывод счетов с суммой на вкладе больше N, введите N:");
            int n = Convert.ToInt32(Console.ReadLine());

            var summan = from sn in historyaccountuser
                         where sn.Summa > n
                         select sn;

            foreach (var symn in summan)
            {
                Console.WriteLine($"№ счета:{symn.Id} | дата открытия:{symn.DataCh} | сумма:{symn.Summa} | владелец счета, ФИО: {symn.Fam} {symn.Name} {symn.Otch}");
            }
        }     
    }
}
